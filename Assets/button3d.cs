using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class button3d : MonoBehaviour
{
    public UnityEvent m_MyEvent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenUrl(string url)
    {
        Application.OpenURL(url);
    }

    void OnMouseDrag()
    {
        m_MyEvent.Invoke();
    }
    
}
